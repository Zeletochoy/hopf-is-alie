#include "Demo.hh"

#include <string>
#include <memory>
#include "GL.hh"

std::unique_ptr<Demo> Demo::instance;

Demo& Demo::getInstance()
{
    if (!instance)
        instance = std::make_unique<Demo>();
    return *instance;
}

Demo::Demo()
  : window(nullptr)
  , vertexArrayID(0)
  , begin(std::chrono::high_resolution_clock::now())
{
}

Demo::~Demo()
{
    glDeleteProgram(programID);
    glDeleteBuffers(1, &quad);
    glDeleteVertexArrays(1, &vertexArrayID);
    delete window;
}

void Demo::launch()
{
    init();
    begin = std::chrono::high_resolution_clock::now();
    while (running())
    {
        update();
        window->clear();
        render();
        window->swapBuffers();
    }
}

void Demo::reloadShader()
{
    programID = GL::loadShader("src/shader.vert",
                               "src/shader.frag");
}

void Demo::init()
{
    window = new Window(800, 600, "ALIE");

    glGenVertexArrays(1, &vertexArrayID);
    glBindVertexArray(vertexArrayID);

    glGenBuffers(1, &quad);

    static const GLfloat quad_vertex_buffer[] = {
        -1.0f,  1.0f, 0.0f,
        1.0f,   1.0f, 0.0f,
        -1.0f, -1.0f, 0.0f,

        -1.0f, -1.0f, 0.0f,
        1.0f,   1.0f, 0.0f,
        1.0f,  -1.0f, 0.0f
    };

    glBindBuffer(GL_ARRAY_BUFFER, quad);
    glBufferData(GL_ARRAY_BUFFER, sizeof (quad_vertex_buffer), quad_vertex_buffer, GL_STATIC_DRAW);


    reloadShader();
}

void Demo::update()
{
    static size_t lastTime = elapsedTime();
    window->poll_events();
    ++frames;
}

void Demo::render()
{
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, quad);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    glUseProgram(programID);
    glUniform2f(glGetUniformLocation(programID, "iResolution"), window->get_w(), window->get_h());
    glUniform1f(glGetUniformLocation(programID, "iGlobalTime"), elapsedTime() / 1000.0f);
    glUniform1f(glGetUniformLocation(programID, "FPS"), FPS);

    glDrawArrays(GL_TRIANGLES, 0, 6);
    glDisableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

bool Demo::running()
{
    return !window->should_close();
}

size_t Demo::elapsedTime()
{
    //auto now = std::chrono::high_resolution_clock::now() - begin;
    //return std::chrono::duration_cast<std::chrono::milliseconds>(now).count();
    return 1000 * frames / 30.0;
}
