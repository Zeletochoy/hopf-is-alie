#pragma once

#include <chrono>
#include <vector>
#include <memory>

#include "Window.hh"

class Demo
{
    public:
        static Demo& getInstance();
        void launch();
        void reloadShader();

        Demo();
        ~Demo();

    private:
        void init();
        void update();
        void render();
        bool running();
        size_t elapsedTime();

        static std::unique_ptr<Demo> instance;

        Window* window;
        GLuint vertexArrayID;
        GLuint quad;
        GLuint programID;

        std::chrono::time_point<std::chrono::high_resolution_clock> begin;

        float FPS;
        size_t frames = 0;
};
