#version 330 core

out vec3 fragColor;

uniform vec2 iResolution;
uniform float iGlobalTime;
uniform float FPS;

#define PI 3.141592653589793

vec3 fiber_at(vec3 pt, float ph)
{
  float th = atan(-pt.x, pt.y) - ph;
  float al = sqrt((1.0 + pt.z) / 2.0);
  float be = sqrt((1.0 - pt.z) / 2.0);
  float w = al * cos(th);
  float x = -be * cos(ph);
  float y = -be * sin(ph);
  float z = al * sin(th);
  float r = acos(w) / PI;
  float f = r / (1.0 - w * w);
  return f * vec3(x, y, z);
}

float sdTorus(vec3 p, vec2 t)
{
  return length(vec2(length(p.xz) - t.x, p.y)) - t.y;
}

vec3 opTurn(vec3 p, vec3 rot)
{
  float a = -rot.x, b = -rot.y, c = -rot.z;
  float ca = cos(a), cb = cos(b), cc = cos(c);
  float sa = sin(a), sb = sin(b), sc = sin(c);
  mat3 m = mat3(ca*cb*cc-sa*sc, -ca*cb*sc-sa*cc, ca*sb,
                sa*cb*cc+ca*sc, -sa*cb*sc+ca*cc, sa*sb,
                -sb*cc,         sb*sc,           cb);
  return m * p;
}

vec3 calc_rot(vec3 norm)
{
  float x = atan(norm.y, norm.z);
  float y = atan(norm.x, norm.y);
  float z = atan(norm.y, norm.x);
  return vec3(x, y, z);
}

float fiber(vec3 p, vec3 pt)
{
  vec3 a = fiber_at(pt, 0.0);
  vec3 b = fiber_at(pt, PI);
  vec3 c = fiber_at(pt, 0.5 * PI);
  vec3 center = (a + b) / 2.0;
  vec3 norm = normalize(cross(c - center, b - center));
  norm = normalize(cross(b - center, norm));
  vec3 rot = calc_rot(norm);
  vec2 torus_params = vec2(1.0, 0.01);
  return sdTorus(opTurn(p, rot) + center, torus_params);
}

vec3 opRep(vec3 p, vec3 c)
{       
  float len = length(p);
  p = normalize(p);

  float delta = PI / 8.;

  vec3 pol = vec3(fract(len),
                  mod(atan(p.y, p.x), delta) - delta / 2.,
                  atan(p.z, length(p.xy)));

  return vec3(pol.x * cos(pol.y)* cos(pol.z),
              pol.x * sin(pol.y) * cos(pol.z),
              pol.x * sin(pol.z));
}

float equator_fibers(vec3 p)
{
  float d = 9999999999999.0;
  for (float th = 0.1; th < 2.0 * PI; th += PI / 32.0)
    d = min(d, fiber(p, normalize(vec3(cos(th), sin(th), 0.0))));
  return d;
}

//----------------------------------------------------------------------

vec2 map(in vec3 pos)
{
  //return vec2(sdTorus(opRep(pos, vec3(1., 1., 0.)), vec2(0.5, 0.02)), 1.0);
  return vec2(equator_fibers(pos), 1.0);
}

vec2 castRay( in vec3 ro, in vec3 rd )
{
  float tmin = 1.0;
  float tmax = 20.0;

  float precis = 0.002;
  float t = tmin;
  float m = -1.0;
  int count = 0;
  for(int i=0; i < 50; i++)
  {
    count++;
    vec2 res = map( ro+rd*t );
    if( res.x<precis || t>tmax ) break;
    t += res.x;
    m = res.y;
  }

  if( t>tmax ) m=-1.0;
  return vec2( t, count);
}


float softshadow( in vec3 ro, in vec3 rd, in float mint, in float tmax )
{
  float res = 1.0;
  float t = mint;
  for( int i=0; i<16; i++ )
  {
    float h = map( ro + rd*t ).x;
    res = min( res, 8.0*h/t );
    t += clamp( h, 0.02, 0.10 );
    if( h<0.001 || t>tmax ) break;
  }
  return clamp( res, 0.0, 1.0 );

}

vec3 calcNormal( in vec3 pos )
{
  vec3 eps = vec3( 0.001, 0.0, 0.0 );
  vec3 nor = vec3(
                  map(pos+eps.xyy).x - map(pos-eps.xyy).x,
                  map(pos+eps.yxy).x - map(pos-eps.yxy).x,
                  map(pos+eps.yyx).x - map(pos-eps.yyx).x );
  return normalize(nor);
}

float calcAO( in vec3 pos, in vec3 nor )
{
  float occ = 0.0;
  float sca = 1.0;
  for( int i=0; i<5; i++ )
  {
    float hr = 0.01 + 0.12*float(i)/4.0;
    vec3 aopos =  nor * hr + pos;
    float dd = map( aopos ).x;
    occ += -(dd-hr)*sca;
    sca *= 0.95;
  }
  return clamp( 1.0 - 3.0*occ, 0.0, 1.0 );    
}

vec3 render( in vec3 ro, in vec3 rd )
{ 
  vec3 col = vec3(0.0, 0.3, 1.0);
  vec2 res = castRay(ro,rd);
  float t = res.x;
  float m = res.y;
  if( m>-0.5 )
  {
    vec3 pos = ro + t*rd;
    vec3 nor = calcNormal( pos );
    vec3 ref = reflect( rd, nor );

    // material        
    col = 0.35 + 0.3 * cos(vec3(0.05,0.08,0.10) * 42. * log(m));

    // lighitng        
    float occ = calcAO( pos, nor );
    vec3  lig = normalize( vec3(-0.6, 0.7, -0.5) );
    float amb = clamp( 0.5+0.5*nor.y, 0.0, 1.0 );
    float dif = clamp( dot( nor, lig ), 0.0, 1.0 );
    float bac = clamp( dot( nor, normalize(vec3(-lig.x,0.0,-lig.z))), 0.0, 1.0 )*clamp( 1.0-pos.y,0.0,1.0);
    float dom = smoothstep( -0.1, 0.1, ref.y );
    float fre = pow( clamp(1.0+dot(nor,rd),0.0,1.0), 2.0 );
    float spe = pow(clamp( dot( ref, lig ), 0.0, 1.0 ),16.0);

    dif *= softshadow( pos, lig, 0.02, 2.5 );
    dom *= softshadow( pos, ref, 0.02, 2.5 );

    /*vec3 brdf = vec3(0.0);
      brdf += 1.20*dif*vec3(1.00,0.90,0.60);
      brdf += 1.20*spe*vec3(1.00,0.90,0.60)*dif;
      brdf += 0.30*amb*vec3(0.50,0.70,1.00)*occ;
      brdf += 0.40*dom*vec3(0.50,0.70,1.00)*occ;
      brdf += 0.30*bac*vec3(0.25,0.25,0.25)*occ;
      brdf += 0.40*fre*vec3(1.00,1.00,1.00)*occ;
      brdf += 0.02;
      col = col*brdf;*/

    col = mix( col, vec3(1.0, 0.0, 0.0), 1.0-exp( -0.0005*t*t ) );

  }

  return vec3( clamp(col,0.0,1.0) );
}

mat3 setCamera( in vec3 ro, in vec3 ta, float cr )
{
  vec3 cw = normalize(ta-ro);
  vec3 cp = vec3(sin(cr), cos(cr),0.0);
  vec3 cu = normalize( cross(cw,cp) );
  vec3 cv = normalize( cross(cu,cw) );
  return mat3( cu, cv, cw );
}

void main()
{
  vec2 fragCoord = gl_FragCoord.xy;
  vec2 q = fragCoord.xy/iResolution.xy;
  vec2 p = -1.0+2.0*q;
  p.x *= iResolution.x/iResolution.y;

  float time = iGlobalTime;

  // camera     
  vec3 ro = 4.0 * vec3(cos(time), 0.0, sin(time));
  vec3 ta = vec3(0.);

  // camera-to-world transformation
  mat3 ca = setCamera( ro, ta, 0.0 );

  // ray direction
  vec3 rd = ca * normalize( vec3(p.xy,2.0) );

  // render   
  vec3 col = render( ro, rd );

  fragColor = pow( col, vec3(0.4545) );
}
